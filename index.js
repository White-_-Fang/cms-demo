const express = require('express');
const path = require('path');
const bodyParser = require('body-parser');
const fileUpload = require('express-fileupload');
const session = require('express-session');

var myApp = express();
myApp.use(bodyParser.urlencoded({ extended: false }));
myApp.set('views', path.join(__dirname, 'views'));
myApp.use(express.static(__dirname + '/public'));
myApp.set('view engine', 'ejs');
myApp.use(fileUpload());
myApp.use(session({
     secret: 'randomstring',
     resave: false,
     saveUninitialized: true
}));

const mongoose = require('mongoose');
mongoose.connect('mongodb://localhost:27017/stephencorp', {
     useNewUrlParser: true,
     useUnifiedTopology: true
});

var Page = mongoose.model('Page', {
     heading: String,
     pageDesc: String,
     heroImageName: String
});

var Header = mongoose.model('Header', {
     title: String,
     logoImageName: String
});

var Admin = mongoose.model('Admin', {
     username: String,
     password: String
});

myApp.get('/', function (req, res) {
     if (req.session.userLoggedIn) {
          res.render('admin');
     }
     else {
          res.redirect('/login');
     }
});

myApp.get('/login', function (req, res) {
     res.render('login');
});

myApp.post('/login', function (req, res) {
     var username = req.body.username;
     var password = req.body.password;

     Admin.findOne({ username: username, password: password }).exec(function (err, admin) {
          if (admin) {
               req.session.username = admin.username;
               req.session.userLoggedIn = true;
               res.redirect('/');
          }
          else
               res.render('login', { error: 'Sorry, cannot login.' });
     });
});

myApp.get('/logout', function (req, res) {
     req.session.username = "";
     req.session.userLoggedIn = false;
     res.render('login', { error: 'Successfully logged out' });
});

myApp.get('/addpage', function (req, res) {
     res.render('addpage');
});

myApp.get('/editheader', function (req, res) {
     res.render('editheader');
});

myApp.post('/editheader', function (req, res) {
     var title = req.body.title;

     var logoImageName = req.files.logoImage.name;
     var logoImage = req.files.logoImage;
     var logoImagePath = 'public/user_images/' + logoImageName;
     logoImage.mv(logoImagePath, function (err) {
          console.log(err);
     });

     var headerData = {
          title: title,
          logoImageName: logoImageName
     };

     var newHeader = new Header(headerData);
     newHeader.save();

     res.render('headersuccess');
});

myApp.post('/addpage', function (req, res) {
     var heading = req.body.heading;
     var pageDesc = req.body.textInput;

     var heroImageName = req.files.heroImage.name;
     var heroImage = req.files.heroImage;
     var heroImagePath = 'public/user_images/' + heroImageName;
     heroImage.mv(heroImagePath, function (err) {
          console.log(err);
     });

     var pageData = {
          heading: heading,
          heroImageName: heroImageName,
          pageDesc: pageDesc
     };

     var newPage = new Page(pageData);
     newPage.save();

     res.render('pagesuccess');
});

myApp.get('/getHome', function (req, res) {
     Header.find({}).exec(function (err, headers) {
          if (req.xhr) {
               return res.status(200).json({
                    headers: headers
               });
          }
     });
});

myApp.get('/getNav', function (req, res) {
     Page.find({}).exec(function (err, navPages) {
          if (req.xhr) {
               return res.status(200).json({
                    navPages: navPages
               });
          }
     });
});

myApp.get('/pages/:pageId', function (req, res) {
     var pageId = req.params.pageId;

     Page.findOne({ heading: pageId }).exec(function (err, pages) {
          res.render('template', { pages: pages });
     });
});

myApp.get('/editpage', function (req, res) {
     Page.find({}).exec(function (err, pages) {
          res.render('editpage', { pages: pages });
     });
});

myApp.get('/delete/:id', function (req, res) {
     if (req.session.userLoggedIn) {
          var id = req.params.id;
          Page.findByIdAndDelete({ _id: id }).exec(function (err, pages) {
               if (pages)
                    res.render('delete', { message: 'Successfully deleted' });
               else
                    res.render('delete', { message: 'Unable to delete' });
          });
     }
     else
          res.redirect('/login');
});

myApp.get('/edit/:pageID', function (req, res) {
     if (req.session.userLoggedIn) {
          var pageID = req.params.pageID;
          console.log(pageID);
          Page.findOne({ _id: pageID }).exec(function (err, editPage) {
               if (editPage)
                    res.render('edit', { editPage: editPage });
               else
                    res.send('No such page found');
          });
     }
     else
          res.redirect('/login');
});

myApp.listen(8080);
console.log('Website running on Port: 8080...');